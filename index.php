<?php

// Define se o ambiente é desenvolvimento (development) ou produção (production).
$environmet = 'development';

//Se o ambiente for desenvolvimento, reporta todos os erros do php, se não desabilita os erros.
if($environmet == 'development') {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
} else {
    error_reporting(0);
    ini_set('display_errors', '0');
}

// Seta o time zone.
date_default_timezone_set('America/Sao_Paulo');

// Seta os cookies para acessar apenas por http.
ini_set('session.cookie_httponly', 1);

// Carrega o autoload do composer.
require 'vendor/autoload.php';

// Carrega o arquivo de constantes.
require 'app/config/constants.php';

// Carrega o arquivo de configurações.
require 'app/config/config.php';

// Carrega o arquivo de rotas.
require 'app/config/routes.php';

// Se "$config['maintenance']" for true, mostra a mensagem de sistema em manutenção.
if ($config['maintenance']) {
    die('<h3>Sistema em manutenção, volte mais tarde.</h3>');
}

// Carrega o arquivo Bootstrap.
require_once 'core/Bootstrap.php';

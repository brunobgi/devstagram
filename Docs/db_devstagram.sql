-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.36-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela db_devstagram.photos
CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(100) NOT NULL,
  `added_in` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_devstagram.photos: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` (`id`, `user_id`, `url`, `added_in`) VALUES
	(1, 1, 'a1b2c3', '0000-00-00 00:00:00'),
	(2, 1, '042020', '0000-00-00 00:00:00'),
	(3, 2, '2552', '0000-00-00 00:00:00'),
	(4, 2, '899563', '0000-00-00 00:00:00'),
	(5, 2, '412563', '0000-00-00 00:00:00'),
	(6, 3, '986565', '0000-00-00 00:00:00'),
	(7, 3, '5642', '0000-00-00 00:00:00'),
	(8, 2, 'abc1234', '2019-03-16 20:22:36');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;

-- Copiando estrutura para tabela db_devstagram.photos_comments
CREATE TABLE IF NOT EXISTS `photos_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `added_in` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_devstagram.photos_comments: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `photos_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `photos_comments` ENABLE KEYS */;

-- Copiando estrutura para tabela db_devstagram.photos_likes
CREATE TABLE IF NOT EXISTS `photos_likes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `photo_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_devstagram.photos_likes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `photos_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `photos_likes` ENABLE KEYS */;

-- Copiando estrutura para tabela db_devstagram.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `added_in` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_devstagram.users: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `avatar`, `added_in`) VALUES
	(1, 'Bruno Oliveira', 'bholiveira.web@gmail.com', '$2y$10$EQRc4/3WRorfXPZedidp/OaRw/dxZ4xTR55cxNFVIiLmZT8zfuqr6', NULL, '2019-03-16 20:24:49'),
	(2, 'Jhon Doe', 'jhondoe@example.com', '$2y$10$qRfw7uqksk8FJXn9iTxVQ.goC27.c7CgttMPBRgnqtvCc2cRjZKGi', NULL, '2019-03-16 20:24:49'),
	(3, 'Admin', 'admin@example.com', '$2y$10$69OiuKn3ukeuZBKEkotAouNuIX5byHk6dkMQcDWYr5lEI2hHq8o7e', NULL, '2019-03-16 20:24:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Copiando estrutura para tabela db_devstagram.users_follow
CREATE TABLE IF NOT EXISTS `users_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_following_id` int(11) NOT NULL,
  `user_follower_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_devstagram.users_follow: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `users_follow` DISABLE KEYS */;
INSERT INTO `users_follow` (`id`, `user_following_id`, `user_follower_id`) VALUES
	(1, 1, 2),
	(2, 1, 3),
	(3, 2, 3),
	(4, 3, 1);
/*!40000 ALTER TABLE `users_follow` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

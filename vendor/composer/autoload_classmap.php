<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Core\\Controller' => $baseDir . '/core/Controller.php',
    'Core\\Core' => $baseDir . '/core/Core.php',
    'Core\\Database' => $baseDir . '/core/Database.php',
    'Core\\Model' => $baseDir . '/core/Model.php',
    'Models\\Users' => $baseDir . '/app/models/Users.php',
);

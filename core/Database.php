<?php

namespace Core;

// Restring o acesso direto ao script pela URL
if (strcmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__)) === 0)
{
    die('Acesso proibido.');
}

/**
 * Define a conexão utilizando o PDO
 * @author Bruno Henrique
 */
class Database
{

    // Propriedade que recebe a conexão
    protected static $db;

    private function __construct()
    {
        // Carrega o arquivo de configurações da base de dados
        require CONFIG_PATH . 'database.php';
        
        try {
            // Recebe as configurações da base de dados
            $database = (object) [
                'host'   => $database['hostname'],
                'name'   => $database['name'],
                'user'   => $database['user'],
                'password'   => $database['password']
            ];
            
            // Conexão PDO
            $dsn = "mysql:host={$database->host}; dbname={$database->name}";
            self::$db = new \PDO($dsn, $database->user, $database->password);
            self::$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            self::$db->exec('SET NAMES utf8');
        } catch (\PDOException $e) {
            // Se houver erro de conexão, gera uma exception
            die("Erro de conexão com o banco de dados: <b>{$e->getMessage()}</b>");
        }
    }
    
    /**
     * Padrão de acesso Singleton
     * @return Class $db
     */
    public static function connect()
    {
        if (!self::$db)
        {
            new Database();
        }

        return self::$db;
    }

}

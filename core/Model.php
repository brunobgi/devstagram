<?php

namespace Core;

// Restring o acesso direto ao script pela URL
if (strcmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__)) === 0)
{
    die('Acesso proibido.');
}

/**
 * <b>Model</>
 * Todos os models devem extender esse Model
 * 
 * @author Bruno Henrique
 */
abstract class Model
{

    // Propriedade que recebe a conexão com a base de dados
    protected $db;

    public function __construct()
    {
        // Retorna a conexão com a base de dados
        $this->db = \Core\Database::connect();
    }

}

<?php

namespace Core;

// Restring o acesso direto ao script pela URL
if (strcmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__)) === 0) {
    die('Acesso proibido.');
}

/**
 * <b>Core<b>
 * Classe principal do Framework (não modificar essa classe)
 *
 * @author Bruno Henrique
 */
class Core
{
    // Propriedade que recebe a URL acessada
    private $current;

    // Propriedade que recebe o controller
    private $controller;

    // Propriedade que recebe a action
    private $action;

    // Propriedade que recebe os parâmetros passados na URL
    private $params = [];

    /**
     * Retorna um array da URL
     * Obtém o controller, a action e os parâmetro
     *
     * @return array $url_exploded
     */
    private function getUrl()
    {
        // Carrega o arquivo de configurações
        require CONFIG_PATH . 'config.php';

        // Pega o controller principal informado no arquivo config.php
        $this->current = $config['controller_default'];

        // Pega a URL atual
		$url = filter_input(INPUT_SERVER, 'PATH_INFO');

		// Se a URL for diferente de null, recebe a URL atual e faz o tratamento
		if ($url) {
			$this->current = filter_var($url, FILTER_SANITIZE_STRING);
        }

        // Checa se existe uma rota para a URL e retorna
        $route = $this->checkRoutes($this->current);

        // Gera um array da URL
        $array = explode('/', $route);

        //Retira valores nulos ou vazios
        $array = array_filter($array);

        // Recria os índices e retorna o array
        return array_values($array);
    }

    /**
     * Valida e trata o controller, action e parâmetros
     *
     * @param string $url
     *
     * @throws \Exception
     */
    public function run()
    {
        try {
            // Pega os dados passados na URL
            $url = $this->getUrl();

            // Pega o controller
            $this->controller = ucwords($url[0]);

            // Trata para aceitar somente strings no nome do controller
            $this->controller = preg_replace('/[^a-zA-Z]/i', '', $this->controller);

            // adiciona o sufixo "Controller" no nome do controller
            $this->controller = $this->controller.'Controller';

            // Pega a action
            $this->action = isset($url[1]) && $url[1] !== '' ? $url[1] : 'index';

            // Destrói os índices 0 e 1 da URL
            unset($url[0], $url[1]);

            // Seta o caminho e nome do controller
            $controller_path = CONTROLLER_PATH."{$this->controller}.php";

            // Se o controller não existir, renderiza a página 404
            if (!file_exists($controller_path)) {
                $this->renderError(array('Not Found'));
            }

            // Carrega o arquivo do controller
            require_once $controller_path;

            // Instancia o controller
            $this->controller = new $this->controller();

            // Seta os parâmetros
            $this->params = isset($url) ? array_values($url) : [];

            // Se a action não existir no controller, renderiza vazio
            if (!method_exists($this->controller, $this->action)) {
                $this->renderError(array('Not Found'));
            }

            // Carrega o controller, a action e os parâmetros
            call_user_func_array([$this->controller, $this->action], $this->params);
            
        } catch (\Throwable $e) {

            $log = "[".date('Y-m-d H:i:s')."] - ".$e->getMessage()." on line ".$e->getLine()." (".$e->getFile().")\n";

            file_put_contents('./error.log', $log, FILE_APPEND);

            // renderiza vazio
            $this->renderError(array('Exception' => $e->getMessage()));
        }
    }

    /**
     * Gera a view que renderiza o erro
     *
     * @return void
     */
    private function renderError($data)
    {
        header("Content-Type: application/json");
        echo json_encode($data);
        exit;
    }

    /**
     * Faz a verificação das rotas passadas em config/routes.php
     * 
     */
    private function checkRoutes($url)
    {
        // Carrega o arquivo de rotas
        require CONFIG_PATH . 'routes.php';

        foreach($routes as $pt => $newurl) {

            // Identifica os argumentos e substitui por regex
            $pattern = preg_replace('(\{[a-z0-9]{1,}\})', '([a-z0-9-]{1,})', $pt);
            
            // Faz o match da URL            
            if(preg_match('#^('.$pattern.')*$#i', $url, $matches) === 1) {

                array_shift($matches);
                array_shift($matches);

                // Pega todos os argumentos para associar
                $itens = array();
                if(preg_match_all('(\{[a-z0-9]{1,}\})', $pt, $m)) {
                    $itens = preg_replace('(\{|\})', '', $m[0]);
                }

                // Faz a associação
                $args = array();
                foreach($matches as $key => $value) {
                    $args[$itens[$key]] = $value;
                }

                // Monta a nova URL
                foreach($args as $argkey => $argvalue) {
                    $newurl = str_replace(':'.$argkey, $argvalue, $newurl);
                }

                $url = $newurl;
                break;
            }
        }

        return $url;
    }
}

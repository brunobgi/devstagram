<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");

// Seta o nome da sessão
session_name(TOKEN_SESSION);

// Seta o tempo de expiração da sessão
session_cache_expire(10);

// Inicia a sessão
session_start();

// Inicia a aplicação
$app = new \Core\Core();
$app->run();
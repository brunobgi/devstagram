<?php
namespace Core;

// Restring o acesso direto ao script pela URL
if (strcmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__)) === 0) {
    die('Acesso proibido.');
}

abstract class Controller
{
    
    protected function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    protected function getRequestData()
    {
        switch ($this->getMethod()) {
            case 'GET':
                return $_GET;
                break;            
            case 'PUT':
            case 'DELETE':
                parse_str(file_get_contents('php://input'), $data);
                return (array) $data;
                break;            
            case 'POST':
                $data = json_decode(file_get_contents('php://input'));
                if (is_null($data)) {
                    $data = $_POST;
                }
                return (array) $data;
                break;
        }
    }

    protected function returnJson($data)
    {
        header("Content-Type: application/json");
        echo json_encode($data);
        exit;
    }

    protected function loadModel(string $model)
    {
        $model = "Models\\" . $model;
        return new $model;
    }
}

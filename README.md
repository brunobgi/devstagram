# Micro Framework PHP MVC
Micro Framework para utilização em projetos PHP, utilizando o padrão MVC.

## Instruções

### Banco de dados
Configurar dados de acesso do banco de dados no arquivo app/config/database.php
```sh
// Host da base de dados
$database['hostname'] = "localhost";

// Nome da base de dados
$database['name'] = "db_mvc";

// Usuário da base de dados
$database['user'] = "root";

// Senha da base de dados
$database['password'] = "";
```

### Config
Configurar no arquivo app/config/contants.php
```sh
// URL base da aplicação, utilizada para imagens, javascript, css, etc
define('BASE_URL', 'http://localhost/mvc-php/');
```

### Controllers
Os controllers devem ser colocados dentro da pasta app/controllers, com a primeira letra em maiúscula com sufixo "Controller" e a classe deve seguir o mesmo padrão, sendo referenciado na url, por exemplo: http://example.com/example. Todos os controllers devem estender o Controller padrão do framework.

##### Exemplo de utilização: 
Crie o arquivo da classe dentro a pasta app/controllers; 
Declarar a classe da seguinte maneira:
```sh
class ExampleController extends \Core\Controller
{
    ...
}
```

### Actions
As actions devem ser declarados como métodos nos controllers sendo referenciado na url, caso a action esteja vazia, por padrão o framework assume a action "index", por exemplo: http://example.com/example/action.

##### Exemplo de utilização: 
Dentro do controller, criar o método a ser utilizado, caso esteja vazio o padrão será o index:
```sh
class ExampleController extends \Core\Controller
{
    public function index(){
        ...
    }

    public function action(){
        ...
    }
}
```

### Parametros
Tudo o que vier após a action, será considerado como parâmetro para utilização dentro da action, por exemplo: http://example.com/example/action/1/2.

##### Exemplo de utilização: 
```sh
class ExampleController extends \Core\Controller
{
    public function index(){
        ...
    }

    public function action($param1, $param2){
        ...
    }
}
```

### Views
Para utilização de uma view, é necessário que o arquivo esteja dentro da pasta app/views e invocar o método $this->render(), que uitliza o [twig](https://twig.symfony.com/) como template engine.

##### Exemplo de utilização: 
```sh
class ExampleController extends \Core\Controller
{
    public function index(){
        $this->render('example.twig');
    }
}
```

### Models
Os Models devem ser colocados dentro da pasta app/models, com a primeira letra em maiúscula, por exemplo: http://example.com/example. Todos os Models devem estender o Model padrão do framework.

##### Exemplo de utilização: 
Crie o arquivo da classe dentro a pasta app/models; 
Declarar a classe da seguinte maneira:
```sh
class Example extends \Core\Model
{
    ...
}
```

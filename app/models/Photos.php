<?php
namespace Models;

use Core\Model;

class Photos extends Model
{
    public function getPhotosCount($user_id)
    {
        $sql = "SELECT COUNT(*) as c FROM photos WHERE user_id = :user_id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(':user_id', $user_id);
        $sql->execute();
        $result = $sql->fetch();

        return $result['c'];
    }

    public function getFeedCollection($following_users, $offset, $perpage)
    {
        $result = array();

        if (count($following_users) > 0) {
            $sql = "SELECT * FROM photos WHERE user_id in (" . implode(', ', $following_users) . ") ORDER BY id DESC LIMIT " . $offset . ", " . $perpage;
            $sql = $this->db->query($sql);

            if ($sql->rowCount() > 0) {
                $result = $sql->fetchAll(\PDO::FETCH_ASSOC);
            }
        }

        return $result;
    }

    public function deleteAll($user_id)
    {
        /**
         * Deleta os comentários das fotos do usuário $user_id
         */
        $sql = "DELETE FROM photos_comments WHERE user_id = :user_id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(':user_id', $user_id);
        $sql->execute();

        /**
         * Deleta os likes das fotos do usuário $user_id
         */
        $sql = "DELETE FROM photos_likes WHERE user_id = :user_id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(':user_id', $user_id);
        $sql->execute();

        /**
         * Deleta as fotos do usuário $user_id
         */
        $sql = "DELETE FROM photos WHERE user_id = :user_id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(':user_id', $user_id);
        $sql->execute();

        return;
    }
}
<?php
namespace Models;

use Core\Model;
use Models\Jwt;
use Models\Photos;

class Users extends Model
{
    private $user_id;

    public function checkCredentials($email, $password)
    {
        $sql = "SELECT id, password FROM users WHERE email = :email";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(':email', $email);
        $sql->execute();
        
        if ($sql->rowCount() > 0) {
            $result = $sql->fetch();

            if (password_verify($password, $result['password'])) {
                $this->user_id = $result['id'];
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getMe()
    {
        return $this->user_id;
    }

    public function getFeed($offset = 0, $perpage = 10)
    {
        $following_users = $this->getFollowingUsers($this->getMe());

        $photos = new Photos();
        return $photos->getFeedCollection($following_users, $offset, $perpage);
    }

    public function save($name, $email, $password)
    {
        if (!$this->emailExists($email)) {
            $password_hash = password_hash($password, PASSWORD_BCRYPT);

            $sql = "INSERT INTO users (name, email, password) VALUES (:name, :email, :password)";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(':name', $name);
            $sql->bindValue(':email', $email);
            $sql->bindValue(':password', $password_hash);
            $sql->execute();

            $this->user_id = $this->db->lastInsertId();

            return true;
        } else {
            return false;
        }
    }

    public function read($id)
    {
        $result = array();
        $photos = new Photos;

        $sql = "SELECT id, name, email, avatar FROM users WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(':id', $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $result = $sql->fetch(\PDO::FETCH_ASSOC);

            if (!empty($result['avatar'])) {
                $result['avatar'] = BASE_URL . 'media/avatar/' . $result['avatar'];
            } else {
                $result['avatar'] = BASE_URL . 'media/avatar/default.jpg';
            }

            $result['qty_following'] = $this->getFollowingCount($id);
            $result['qty_followers'] = $this->getFollowersCount($id);
            $result['qty_photos'] = $photos->getPhotosCount($id);
        }

        return $result;
    }

    public function edit($user_id, $data)
    {
        if ($user_id === $this->getMe()) {
            $fields = array();

            if (!empty($data['name'])) {
                $fields['name'] = $data['name'];
            }

            if (!empty($data['email'])) {
                if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                    if (!$this->emailExists($data['email'])) {
                        $fields['email'] = $data['email'];
                    } else {
                        return array('error' => 'Esse e-mail já está sendo utilizado.');
                    }
                } else {
                    return array('error' => 'Informe um e-mail válido.');
                }
            }

            if (!empty($data['password'])) {
                $fields['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
            }

            if (count($fields) > 0) {
                foreach ($fields as $key => $value) {
                    $field[] = $key . '= :'.$key;
                }

                $sql = "UPDATE users SET " . implode(',', $field) . " WHERE id = :user_id";
                $sql = $this->db->prepare($sql);
                $sql->bindValue(':user_id', $user_id);

                foreach ($fields as $key => $value) {
                   $sql->bindValue(':' . $key, $value);
                }

                return $sql->execute();

            } else {
                return array('error' => 'Preencha os dados a serem alterados.');
            }
        } else {
            return array('error' => 'Não é permitido editar outros usuários.');
        }

        return array();
    }

    public function delete($user_id)
    {
        if ($user_id === $this->getMe()) {
            $photos = new Photos();
            $photos->deleteAll($user_id);

            $sql = "DELETE FROM users_follow WHERE user_following_id = :user_id OR user_follower_id = :user_id";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(':user_id', $user_id);
            $sql->execute();

            $sql = "DELETE FROM users WHERE id = :user_id";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(':user_id', $user_id);
            $sql->execute();

            return array();
        } else {
            return array('error' => 'Não é permitido excluir outro usuário');
        }
    }

    public function getFollowingCount($user_id)
    {
        $sql = "SELECT COUNT(*) as c FROM users_follow WHERE user_following_id = :user_id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(':user_id', $user_id);
        $sql->execute();
        $result = $sql->fetch();

        return $result['c'];
    }

    public function getFollowingUsers($user_id)
    {
        $result = array();

        $sql = "SELECT user_following_id FROM users_follow WHERE user_follower_id = :user_id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(':user_id', $user_id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
            foreach ($data as $item) {
                $result[] = intval($item['user_following_id']);
            }
        }

        return $result;
    }

    public function getFollowersCount($user_id)
    {
        $sql = "SELECT COUNT(*) as c FROM users_follow WHERE user_follower_id = :user_id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(':user_id', $user_id);
        $sql->execute();
        $result = $sql->fetch();

        return $result['c'];
    }

    public function emailExists($email)
    {
        $sql = "SELECT id FROM users WHERE email = :email";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(':email', $email);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function createJwt()
    {
        $jwt = new Jwt;
        return $jwt->create(array('user_id' => $this->user_id));
    }

    public function validateJwt($token)
    {
        $jwt = new Jwt;
        $token_info = $jwt->validate($token);

        if (isset($token_info->user_id)) {
            $this->user_id = $token_info->user_id;
            return $this->user_id;
        } else {
            return false;
        }
    }
}
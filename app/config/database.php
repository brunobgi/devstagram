<?php

// Restringe o acesso direto ao script pela url
if (strcmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__)) === 0) {
    die('Acesso proibido.');
}

/* Configurações de conexão que serão utilizadas pelo PDO. */

// Host da base de dados
$database['hostname'] = "localhost";

// Nome da base de dados
$database['name'] = "db_devstagram";

// Usuário da base de dados
$database['user'] = "root";

// Senha da base de dados
$database['password'] = "";

<?php

/*** Definição de rotas do sistema ***/
$routes = array();

/*** Rotas de usuários ***/
$routes['/users/login'] = '/users/login';
$routes['/users/create'] = '/users/create';
$routes['/users/feed'] = '/users/feed';
$routes['/users/{id}'] = '/users/view/:id';
$routes['/users/{id}/photos'] = '/users/photos/:id';
$routes['/users/{id}/follow'] = '/users/follow/:id';

/*** Rotas de fotos ***/
$routes['/photos/random'] = '/photos/random';
$routes['/photos/create'] = '/photos/create';
$routes['/photos/{id}'] = '/photos/view/:id';
$routes['/photos/{id}/comment'] = '/photos/comment/:id';
$routes['/photos/{id}/like'] = '/photos/like/:id';
<?php

// Restringe o acesso direto ao script pela url
if (strcmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__)) === 0) {
    die('Acesso proibido.');
}

// Modo de manutenção
$config['maintenance'] = false;

// Controller padrão
$config['controller_default'] = 'home';

// Seta o tema padrão
$config['theme'] = 'default';
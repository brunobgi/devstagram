<?php

// Restringe o acesso direto ao script pela url
if (strcmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__)) === 0) {
    die('Acesso proibido.');
}

// URL base da aplicação, utilizada para imagens, javascript, css, etc
define('BASE_URL', 'http://localhost/devstagram/');

// Pasta onde ficam as configurações, onde ficam armazenadas as configurações
define('CONFIG_PATH', 'app/config/');

// Pasta onde ficam os controllers
define('CONTROLLER_PATH', 'app/controllers/');

// Pasta onde ficam os models
define('MODEL_PATH', 'app/models/');

// Pasta onde ficam as views
define('VIEW_PATH', 'app/views/');

// Pasta onde ficam os helpers
define('HELPER_PATH', 'app/helpers/');

// Pasta onde ficam as bibliotecas
define('LIB_PATH', 'app/libs/');

// JWT Secret Key
define('JWT_SECRET_KEY', '5e8f97603409d38e4405f1f9ecfc168949a43c1a00772c8d37552f7a785a6911');

// Gera um token de sessão
define('TOKEN_SESSION', md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']));

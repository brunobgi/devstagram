<?php
// Restring o acesso direto ao script pela URL
if (strcmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__)) === 0) {
    die('Acesso proibido.');
}

use Core\Controller;

class UsersController extends Controller
{
    public function index()
    {
    }

    public function login()
    {
        $response = array('error' => '');
        $method = $this->getMethod();
        $data = $this->getRequestData();

        if ($method == 'POST') {
            if (!empty($data['email']) && !empty($data['password'])) {
                $users = $this->loadModel('Users');

                if ($users->checkCredentials($data['email'], $data['password'])) {
                    // Gerar o JWT
                    $response['jwt'] = $users->createJwt();
                } else {
                    $response['error'] = 'Acesso negado.';
                }
            } else {
                $response['error'] = 'E-mail e/ou senha não informado.';
            }
        } else {
            $response['error'] = 'Método de requisição incompatível.';
        }

        $this->returnJson($response);
    }

    public function create()
    {
        $response = array('error' => '');
        $method = $this->getMethod();
        $data = $this->getRequestData();

        if ($method == 'POST') {
            if (!empty($data['name']) && !empty($data['email']) && !empty($data['password'])) {
                if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                    $users = $this->loadModel('Users');

                    if ($users->save($data['name'], $data['email'], $data['password'])) {
                        // Gerar o JWT
                        $response['jwt'] = $users->createJwt();
                    } else {
                        $response['error'] = 'E-mail já utilizado.';
                    }
                } else {
                    $response['error'] = 'E-mail inválido.';
                }
            } else {
                $response['error'] = 'Dados necessários não preenchidos.';
            }
        } else {
            $response['error'] = 'Método de requisição incompatível.';
        }

        $this->returnJson($response);
    }

    public function view($id)
    {
        $response = array('error' => '', 'logged' => false, 'is_me' => false);
        $method = $this->getMethod();
        $data = $this->getRequestData();

        $users = $this->loadModel('Users');

        if (!empty($data['jwt']) && $users->validateJwt($data['jwt'])) {
            $response['logged'] = true;

            if ($id == $users->getMe()) {
                $response['is_me'] = true;
            }
            
            switch ($method) {
                case 'GET':
                    $response['data'] = $users->read($id);
                    if (count($response['data']) === 0) {
                        $response['error'] = 'Usuário não existe.';
                    }
                    break;
                case 'PUT':
                    $response['data'] = $users->edit($id, $data);
                    if (isset($response['data']['error'])) {
                        $response['error'] = $response['data']['error'];
                    }
                    break;
                case 'DELETE':
                    $response['data'] = $users->delete($id);
                    if (isset($response['data']['error'])) {
                        $response['error'] = $response['data']['error'];
                    }
                    break;
                default:
                    $response['error'] = 'Método '. $method .' não disponível.';
            }
        } else {
            $response['error'] = 'Acesso negado.';
        }

        $this->returnJson($response);
    }

    public function feed()
    {
        $response = array('error' => '', 'logged' => false, 'is_me' => false);
        $method = $this->getMethod();
        $data = $this->getRequestData();

        $users = $this->loadModel('Users');

        if (!empty($data['jwt']) && $users->validateJwt($data['jwt'])) {
            $response['logged'] = true;
            
            if ($method == 'GET') {
                $offset = 0;
                if (!empty($data['offset'])) {
                    $offset = intval($data['offset']);
                }

                $perpage = 10;
                if (!empty($data['perpage'])) {
                    $perpage = intval($data['perpage']);
                }

                $response['data'] = $users->getFeed($offset, $perpage);
            } else {
                $response['error'] = 'Método '. $method .' não disponível.';
            }
        } else {
            $response['error'] = 'Acesso negado.';
        }

        $this->returnJson($response);
    }
}
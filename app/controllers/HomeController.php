<?php
// Restring o acesso direto ao script pela URL
if (strcmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__)) === 0) {
    die('Acesso proibido.');
}

use Core\Controller;

class HomeController extends Controller
{
    public function index()
    { 
    }
}
